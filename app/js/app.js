'use strict';


// Declare app level module which depends on filters, and services
angular.module('wedo', []).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/tasks', {templateUrl: 'partials/tasks.html', controller: 'PersonController'});
    $routeProvider.otherwise({redirectTo: '/tasks'});
  }]);
