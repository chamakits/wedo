'use strict';

/* Controllers */

function PersonController($scope,$http) {
	$http
		//.get('data/people.json')
		.get('http://localhost:8123/people/')
		.success(function(data){
			$scope.people = data;
		});
	$scope.getTasks = function(name){
		$http
			//.get('data/tasks/'+name+'Tasks.json')
			.get('http://localhost:8123/people-tasks/'+name)
			.success(function(data){
				$scope.tasks=data;
				$scope.currentName = name
			});

	};
	$scope.getTasks('Omar');
	$scope.addTask = function(){
		
		$scope.newUserTask = {
			"Name": $scope.currentName,
			"Task":
			{
			//"Id": 4,
			//"Order":4,
			"Done":false,
			"Title":$scope.text
			}};
		$http.post("http://localhost:8123/add-tasks/", $scope.newUserTask)
			.success(function(data){
				$scope.newUserTask.Task=data;
				$scope.tasks.push($scope.newUserTask.Task);
			});
		$scope.text="";

	};
	
	$scope.getImg = function(done){
		if(done)
		{
			return "img/tick-circle.png"
		}
		else
		{
			return "img/cross-button.png"
		}
	};
	
	$scope.flipTask = function(task){
		$scope.newUserTask = {
			"Name": $scope.currentName,
			"Task": task
		};
		
	
		task.Done = !task.Done;
		$http.post("http://localhost:8123/edit-task/", $scope.newUserTask)
			.success(function(data){
				$scope.newUserTask.Task=data;
				task=$scope.newUserTask.Task
			});

		
	};
}

/*
angular.module('myApp.controllers', []).
  controller('MyCtrl1', [function() {

  }])
  .controller('MyCtrl2', [function() {

  }]);
*/